import express from 'express'
import aboutRouter from './about'
import misionRouter from './mision'
import visionRouter from './vision'
import entrepreneurRouter from './entrepreneur'
import investorRouter from './investor'
import projectRouter from './project'

const router = express.Router()

router.use(aboutRouter)
router.use(misionRouter)
router.use(visionRouter)
router.use(entrepreneurRouter)
router.use(projectRouter)
router.use(investorRouter)



export default router