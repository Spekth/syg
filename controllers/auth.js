require('dotenv').config();
const { response } = require('express');
import Entrepreneur from '../models/entrepreneur';
import Investor from '../models/investor';

// Investor

import bcryptjs from 'bcryptjs';
import generarJWT from '../helpers/generar-jwt'

export default {
  login: async(req,res)=>{
   const { email, password } = req.body;
   try{
    const entrepreneur = await Entrepreneur.findOne({email});
    if(!entrepreneur){
      return res.status(400).json({
        msg: 'Email is not correct'
      })
    }

    const validPassword = bcryptjs.compareSync(password,entrepreneur.password);
    if(!validPassword){
      return res.status(400).json({
        msg:'Password is not correct'  
      })  
    } 
    
    // // Generar el JWT
    // const token = await generarJWT(entrepreneur.id); ORIGINAL
    const token = await generarJWT(entrepreneur.id,entrepreneur.role,entrepreneur.email,entrepreneur.name);
    res.json({
      entrepreneur,
      token   
    })
   }catch(err){
    return res.status(500).json({
      msg:'Hable con el administrador'  
    })
   }
  },
  loginInvestor: async(req,res)=>{
   const { email, password } = req.body;
   try{
    const investor = await Investor.findOne({email});
    if(!investor){
      return res.status(400).json({
        msg: 'Email is not correct'
      })
    }

    const validPassword = bcryptjs.compareSync(password,investor.password);
    if(!validPassword){
      return res.status(400).json({
        msg:'Password is not correct'  
      })  
    } 
    
    // // Generar el JWT
    // const token = await generarJWT(investor.id); ORIGINAL
    const token = await generarJWT(investor.id,investor.role,investor.email,investor.name);
    res.json({
      investor,
      token   
    })
   }catch(err){
    return res.status(500).json({
      msg:'Hable con el administrador'  
    })
   }
  },
  // loginEntrepreneur: async(req,res)=>{
  //  const { email, password } = req.body;
  //  try{
  //   const entrepreneur = await Entrepreneur.findOne({email});
  //   if(!entrepreneur){
  //     return res.status(400).json({
  //       msg: 'Email is not correct'
  //     })
  //   }

  //   const validPassword = bcryptjs.compareSync(password,entrepreneur.password);
  //   if(!validPassword){
  //     return res.status(400).json({
  //       msg:'Password is not correct'  
  //     })  
  //   } 
    
  //   // // Generar el JWT
  //   const token = await generarJWT(entrepreneur.id);
  //   res.json({
  //     entrepreneur,
  //     token   
  //   })
  //  }catch(err){
  //   return res.status(500).json({
  //     msg:'Hable con el administrador'  
  //   })
  //  }
  // },
  
  messageFailed:async (req,res)=>{
    res.send('Error connection')
  }
}