import Project from '../models/project'

export default {
 addProject: async(req,res,next)=>{
   try {
     const body = req.body;
     const project = await Project.create(body);
     res.status(200).json(project);  
   } catch (err) {
     res.status(500).send({
        message: `An error ocurred ${err}`
     }) 
     next(err);
   }
 },
  getProjects: async(req,res,next)=>{
    try {
        const projects = await Project.find({})
        res.status(200).json(projects);  
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    }
  },
  updateProject: async(req,res,next)=>{
    try {
        const { id } = req.params;
        const update  = req.body;
        const project  = await Project.findByIdAndUpdate(id,update,{new:true});
        res.status(200).json(project);
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    }
  },
  deleteProject: async(req,res,next)=>{
    try {
        const { id } = req.params;
        const update  = req.body;
        // const project  = await Project.findByIdAndUpdate(id,{state:false},{new:true});
        const project  = await Project.findByIdAndDelete(id);

        res.status(200).json({message:'Project delete succesfully'});
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    } 
  }  
}