import jwt from 'jsonwebtoken';

const generarJWT = (uid = '',role,email,name) => {
   return new Promise((resolve,reject)=>{
    //  const payload = { uid };  ONLY ID O UID
    //  const payload = { uid}; ORIGINAL
     const payload = { uid,role,email,name};
      jwt.sign(payload,process.env.SECRETORPRIVATEKEY,{
        expiresIn:'4h'  
      },(err,token)=>{
        if(err){
          console.log(err);
          reject('No se pudo generar el token');  
        }else{
          resolve(token);  
        }
      })  
   }) 
}

export default generarJWT; 