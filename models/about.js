import mongoose,{Schema} from 'mongoose'

const AboutSchema = new Schema({
  title: String,
  description: {type:String,default:'somos un emprendimiento que nació de la necesidad de apoyar al emprendedor de la ciudad de montería.Nuestra razón social está dirigida a la prestación de servicios, en el cualfomentamos su crecimiento a traves de asesorías, realización de ferias y posicionamientode marca. Nos caracterizamos por brindar servicios fundamentados en el bienestary satisfación de nuestros clientes.'},
  state:{type:Boolean,default:true}, 
  createAt: {type:Date,default:Date.now()}  
},{versionKey:false})

const About = mongoose.model('About',AboutSchema);

export default About;