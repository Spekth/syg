import mongoose, { Schema } from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
const rolesValidos = {
   values: ['EMPRENDEDOR'],
   message: '{VALUE} is not a valid role' 
}

const EntrepreneurSchema = new Schema({
  name: {type:String,required:['name is required',true]},
  userName: {type:String, required:['Password is required',true]},
  email: {type:String, required:['email is required',true],unique:true},
  phone:{type:String,default:'NOT FOUND'},
  password:{type:String,required:['Password is required',true]},
  role:{type:String,enum:rolesValidos},
  project:{
    title: {type:String,required:['Title project is required',true]},
    description: {type:String,required:['Description project is required',true]},
    image: {type:String,default:'https://res.cloudinary.com/dqhme1rod/image/upload/v1620608157/mwiknqsayji2rpqxnjph.png'}
  },
  state:{type:Boolean,default:true},
  createAt:{type:Date,default:Date.now()}  
},{versionKey:false})

// Ocultar la password en la response
EntrepreneurSchema.methods.toJSON = function(){
  const { password, _id,...user} = this.toObject();
  user.uid = _id;
  return user; 
}

EntrepreneurSchema.plugin(uniqueValidator,{message:'{PATH} must be unique'});
const Entrepreneur = mongoose.model('Entrepreneur',EntrepreneurSchema);

export default Entrepreneur;
