import mongoose, { Schema } from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
const rolesValidos = {
   values: ['EMPRENDEDOR','INVERSIONISTA'],
   message: '{VALUE} is not a valid role' 
}

const UserSchema = new Schema({
  name: {type:String,required:['name is required',true]},
  email: {type:String, required:['email is required',true],unique:true},
  phone:{type:String,default:'NOT FOUND'},
  password:{type:String,required:true},
  userName: {type:String, required:true},
  // role:{type:String,default:'EMPRENDEDOR',enum:rolesValidos,required:true},
  role:{type:String,default:'EMPRENDEDOR',enum:rolesValidos},
  // id collection project
  project: {type:Schema.Types.ObjectId, ref: 'Project',required:false},
  state:{type:Boolean,default:true},
  createAt:{type:Date,default:Date.now()}  
},{versionKey:false})

// Ocultar la password en la response
UserSchema.methods.toJSON = function(){
  const { password, _id,...user} = this.toObject();
  user.uid = _id;
  return user; 
}

UserSchema.plugin(uniqueValidator,{message:'{PATH} must be unique'});
const User = mongoose.model('User',UserSchema);

export default User;
