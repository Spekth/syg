import mongoose,{Schema} from 'mongoose'

const VisionSchema = new Schema({
  description: String,
  createAt: {type:Date,default:Date.now()},
  state:{type:Boolean,default:true}   
},{versionKey:false})

const Vision = mongoose.model('Vision',VisionSchema);

export default Vision;