import mongoose, {Schema} from 'mongoose'

const ProjectSchema = new Schema({
   title: {type:String,required:['title project is required',true]},
   photos:[String],
   description: {type:String, required:['Description is required',true]},
   state:{type:Boolean,default:true},
   createAt:{type:Date, default: Date.now()}

   // CADA PROYECTO DEBE TENER UN EL NOMBRE DEL AUTOR
},{versionKey:false})

const Project = mongoose.model('Project',ProjectSchema)

export default Project; 