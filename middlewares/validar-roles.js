const { response } = require('express');
// INVERSIONISTA
export default {
  roleInversionista: async(req,res,next)=>{
    // if(!req.user){
    if(!req.investor){
        return res.status(500).json({
           msg:'Se quiere verificar el role sin validar el token primero' 
        }); 
      }
      const { role,name } = req.investor;
      if(role!== 'INVERSIONISTA'){
        return res.status(401).json({
          msg: `${ name } is not an inversionista`   
        });  
      }
    
      next(); 
  },
  roleEmprendedor: async(req,res,next)=>{
    if(!req.entrepreneur){
        return res.status(500).json({
           msg:'Se quiere verificar el role sin validar el token primero' 
        }); 
      }
      const { role,name } = req.entrepreneur;
      if(role!== 'EMPRENDEDOR'){
        return res.status(401).json({
          msg: `${ name } is not an emprendedor`   
        });  
      }
    
    next();  
  }  
}
// const roleInversionista = (req,res= response , next) => {
//   if(!req.user){
//     return res.status(500).json({
//        msg:'Se quiere verificar el role sin validar el token primero' 
//     }); 
//   }
//   const { role,name } = req.user;
//   if(role!== 'INVERSIONISTA'){
//     return res.status(401).json({
//       msg: `${ name } is not an inversionista`   
//     });  
//   }

//   next();
// }

// export default roleInversionista;